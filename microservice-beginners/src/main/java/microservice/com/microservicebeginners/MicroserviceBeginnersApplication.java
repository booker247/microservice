package microservice.com.microservicebeginners;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
@SpringBootApplication
@RestController
@EnableEurekaClient
@RequestMapping("/hello")
public class MicroserviceBeginnersApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroserviceBeginnersApplication.class, args);
	}
	@RequestMapping(value="/index",method=RequestMethod.GET)
	  public String helloWorld() {
	     return "Hello world micro service !";
	  }
}
